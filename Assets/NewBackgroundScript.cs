using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBackgroundScript : MonoBehaviour
{
 
    private float screenWidth;

    void Start()
    {
        
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();

        float imageWidth = renderer.sprite.bounds.size.x;
        float imageHeight = renderer.sprite.bounds.size.y;

        float screenHeight = Camera.main.orthographicSize * 2.00f;
        screenWidth = screenHeight / Screen.height * Screen.width;

        Vector2 newScale = this.transform.localScale;

        newScale.x = screenWidth / imageWidth + 0.25f;
        newScale.x = screenHeight / imageHeight;

        this.transform.localScale = newScale;

    }

    void Update()
    {
        
    }
}