using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterScript : MonoBehaviour
{
    public float velocity, jumpForce;
    private bool jump;
    private Rigidbody2D body;
    private SpriteRenderer render;
    private Animator animator;
    void Start()
    {
        this.jump = false;
        
        this.body = GetComponent<Rigidbody2D>();
        this.render = GetComponent<SpriteRenderer>();
        this.animator = GetComponent<Animator>();

        this.animator.enabled = false;
    }

    void Update()
    {
        float x = this.transform.position.x;
        float y = this.transform.position.y;
        float inputKeyHorizontal = Input.GetAxis("Horizontal");
        float inputKeyVertical = Input.GetAxis("Vertical");

        if (inputKeyHorizontal > 0)
        {
            this.body.AddForce(new Vector2(this.velocity, 0));
            this.render.flipX = false;
            this.animator.enabled = true;
        }
        else if (inputKeyHorizontal < 0)
        {
            this.body.AddForce(new Vector2(-this.velocity, 0));
            this.animator.enabled = true;
            this.render.flipX = true;
        }
        else
        {
            this.body.MovePosition(new Vector2(x, y));
            this.animator.enabled = false;
        }
        if (Input.GetButton("Jump") && !this.jump)
        {
            this.jump = true;
            this.body.AddForce(new Vector2(0, this.jumpForce));
            this.animator.enabled = true;
        }
        else if (this.body.velocity.y == 0 && this.jump)
        {
            this.jump = false;
            this.animator.enabled = true;
        }
    }
}